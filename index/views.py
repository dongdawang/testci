"""
Views of app index.
"""
from django.http.response import HttpResponse
from django.views import View


class Index(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse("I am index.")
